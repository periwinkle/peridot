# peridot keyboard

![](img/peridot.jpg)

a keyboard inspired by sofle and atreyu. monoblock 4x6 split, 58 keys, choc hotswap or solder. 2 status leds.

almost the exact same layout as sofle.

uses a nice!nano cause that's what i wanted. could be easily changed to use other microcontrollers but i used
the funny middle pins so it currently doesn't.

see more info about it [on my website](https://periwinkle.sh/projects/peridot/)

![](img/center.jpg)
![](img/comparison.jpg)
